# Lakbay

A Single Page Application for connecting travelers looking for accomodations in specific areas, with people willing to rent out their homes.

Lakbay offers both travellers a convenient platform for finding lodging, and would-be tenants an easy, relatively stress-free way to earn some income from their property. Built using MERN stack (MongoDB, Express, ReactJS, Node.js).

## Usage

1. Run `node ./server.js` in the terminal to turn on the server
2. In `./views`, run `npm start` in the terminal to start the React app.

## Requirements

Use `npm install` in the root folder and in `./views` if needed.
